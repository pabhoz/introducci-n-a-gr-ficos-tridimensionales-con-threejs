var scene,camera,renderer;
var ultiTiempo;
var labels = [];
var objetos = [];
var appW = window.innerWidth * 0.75;
var appH = window.innerHeight;

function webGLStart(){
	iniciarEscena();
	ultiTiempo = Date.now();
	animarEscena();
}

function iniciarEscena(){
	renderer = new THREE.WebGLRenderer();
	renderer.setSize(appW, appH);
	document.body.appendChild(renderer.domElement);

	camera = new THREE.PerspectiveCamera(45, appW/appH, 1, 500);
	camera.position.set(200,200,200);
	camera.lookAt(new THREE.Vector3(0,0,0));

	scene = new THREE.Scene();

	//Iniciar controles de la camara
	controlCamara = new THREE.OrbitControls( camera , renderer.domElement );

	//Grilla!
	var grilla = new THREE.GridHelper( 100, 10 );
	scene.add(grilla);

	var axes = new THREE.AxisHelper(100);
	scene.add(axes);

	var yLabelTexture = textTure(128,"Bold","48px","Arial","255,255,255,1","Y",64,50);
	var yLabel = new THREE.Mesh(new THREE.PlaneGeometry(10,10),
		new THREE.MeshBasicMaterial({
			map: yLabelTexture,
			side:THREE.DoubleSide,
			transparent: true
		})
		);
	yLabel.position.set(0,100,0);
	labels.push(yLabel);
	scene.add(yLabel);

	var xLabelTexture = textTure(128,"Bold","48px","Arial","255,255,255,1","X",64,50);
	var xLabel = new THREE.Mesh(new THREE.PlaneGeometry(10,10),
		new THREE.MeshBasicMaterial({
			map: xLabelTexture,
			side:THREE.DoubleSide,
			transparent: true
		})
		);
	xLabel.position.set(100,0,0);
	labels.push(xLabel);
	scene.add(xLabel);

	var zLabelTexture = textTure(128,"Bold","48px","Arial","255,255,255,1","Z",64,50);
	var zLabel = new THREE.Mesh(new THREE.PlaneGeometry(10,10),
		new THREE.MeshBasicMaterial({
			map: zLabelTexture,
			side:THREE.DoubleSide,
			transparent: true
		})
		);
	zLabel.position.set(0,0,100);
	labels.push(zLabel);
	scene.add(zLabel);

	/**
		Objetos!
		*/
		var vectorColor = 0xCC0099;
		var vMaterial = new THREE.LineBasicMaterial({
			color:vectorColor
		});

		var p0 = new THREE.Vector3(0,0,0);
		var p1 = new THREE.Vector3(15,20,10);
		var vectorp0p1 = twoPointsDistance(p0,p1);
		var uVectorp0p1 = unitario(vectorp0p1); console.log(uVectorp0p1);

		var vGeometry = new THREE.Geometry();
		vGeometry.vertices.push(p0);
		vGeometry.vertices.push(p1);

		var vector = new THREE.Line(vGeometry,vMaterial);

		//scene.add(vector);

		var flechaVector = new THREE.ArrowHelper(uVectorp0p1,p1,1,vectorColor,1,1);

		//scene.add(flechaVector);


	}

	function animarEscena(){
		requestAnimationFrame( animarEscena );
		renderEscena();
		actualizarEscena();
	}

	function renderEscena(){
		renderer.render( scene, camera );
	}

	function actualizarEscena(){
		var delta = (Date.now() - ultiTiempo)/1000;
		ultiTiempo = Date.now();

		for (var i = 0; i < labels.length; i++) {
			labels[i].lookAt(camera.position);
		};

		controlCamara.update();


	}